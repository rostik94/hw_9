const arrowLeft = document.getElementById('arrow-left');
const arrowRight = document.getElementById('arrow-right');
const eachSlide = document.getElementsByClassName('slider-list__item'); 
const dotsClick = document.getElementsByClassName('slider-dots__item');  


arrowLeft.onclick = function() {
    if (eachSlide[0].classList.contains('active') && dotsClick[0].classList.contains('active-dot')) {
            eachSlide[0].classList.remove('active');
            eachSlide[4].classList.add('active');
            dotsClick[0].classList.remove('active-dot');
            dotsClick[4].classList.add('active-dot');
    } else if (eachSlide[4].classList.contains('active') && dotsClick[4].classList.contains('active-dot')) {
            eachSlide[4].classList.remove('active');
            eachSlide[3].classList.add('active');
            dotsClick[4].classList.remove('active-dot');
            dotsClick[3].classList.add('active-dot');
    } else if (eachSlide[3].classList.contains('active') && dotsClick[3].classList.contains('active-dot')) {
            eachSlide[3].classList.remove('active');
            eachSlide[2].classList.add('active');
            dotsClick[3].classList.remove('active-dot');
            dotsClick[2].classList.add('active-dot');
    }  else if (eachSlide[2].classList.contains('active') && dotsClick[2].classList.contains('active-dot')) {
            eachSlide[2].classList.remove('active');
            eachSlide[1].classList.add('active');
            dotsClick[2].classList.remove('active-dot');
            dotsClick[1].classList.add('active-dot');
    }  else if (eachSlide[1].classList.contains('active') && dotsClick[1].classList.contains('active-dot')) {
            eachSlide[1].classList.remove('active');
            eachSlide[0].classList.add('active');
            dotsClick[1].classList.remove('active-dot');
            dotsClick[0].classList.add('active-dot');
    } 
}

arrowRight.onclick = function() {
    if (eachSlide[0].classList.contains('active') && dotsClick[0].classList.contains('active-dot')) {
            eachSlide[0].classList.remove('active');
            eachSlide[1].classList.add('active');
            dotsClick[0].classList.remove('active-dot');
            dotsClick[1].classList.add('active-dot');
    } else if (eachSlide[1].classList.contains('active') && dotsClick[1].classList.contains('active-dot')) {
            eachSlide[1].classList.remove('active');
            eachSlide[2].classList.add('active');
            dotsClick[1].classList.remove('active-dot');
            dotsClick[2].classList.add('active-dot');
    } else if (eachSlide[2].classList.contains('active') && dotsClick[2].classList.contains('active-dot')) {
            eachSlide[2].classList.remove('active');
            eachSlide[3].classList.add('active');
            dotsClick[2].classList.remove('active-dot');
            dotsClick[3].classList.add('active-dot');
    }  else if (eachSlide[3].classList.contains('active') && dotsClick[3].classList.contains('active-dot')) {
            eachSlide[3].classList.remove('active');
            eachSlide[4].classList.add('active');
            dotsClick[3].classList.remove('active-dot');
            dotsClick[4].classList.add('active-dot');
    }  else if (eachSlide[4].classList.contains('active') && dotsClick[4].classList.contains('active-dot')) {
            eachSlide[4].classList.remove('active');
            eachSlide[0].classList.add('active');
            dotsClick[4].classList.remove('active-dot');
            dotsClick[0].classList.add('active-dot');
    } 
}

for ( let a = 0; a < eachSlide.length; a++) {
    dotsClick[a].onclick = function() {
        for ( let i = 0; i < eachSlide.length; i++) {
            if (dotsClick[i].classList.contains('active-dot')) {
                dotsClick[i].classList.remove('active-dot');
                dotsClick[a].classList.add('active-dot');
                eachSlide[i].classList.remove('active');
                eachSlide[a].classList.add('active');
        }
        }
    }
}
